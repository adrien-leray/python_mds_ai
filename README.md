# Projet Python IA

*groupe: Adrien LERAY*

## Description du projet - Sur les routes de SAO PAULO

*url pour recupérer les datas* = https://archive.ics.uci.edu/ml/datasets/Behavior+of+the+urban+traffic+of+the+city+of+Sao+Paulo+in+Brazil

L'objectif de ce projet est de prédire le nombre de morts d'un incident de la route à Sao Paulo en fonction des données fournis par le dataset.

## Générer les graphes avec python

- run `pipenv install`
- run `pipenv shell`
- run `py main.py`


## Méthodes d'apprentissage du prototype orange:

- Méthode des k plus proches voisins
- Forêt d'arbres décisionnels
- Réseau de neurones artificiels
- Arbre de décision

## Prototype orange

Lien vers le prototype: [prototype.ows](prototype.ows)

**Conclusion du prototype et de l'apprentissage:**

Sachant que le dataset ne contient pas beacoup de données et que je n'ai pas réussi à créer l'api et le formulaire pour lui rajouter des données, le prototype montre une précision peu fiable.

![prototype](orange.PNG)

## Graphiques Matplotlib

![figure 1](img/Figure_1.png)
![figure 2](img/Figure_2.png)
![figure 3](img/Figure_3.png)
![figure 4](img/Figure_4.png)






