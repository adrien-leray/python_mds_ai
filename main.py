import pandas as pd
import matplotlib.pyplot as plt
import os

file = r'dataset.csv'

# Récupération des données via le fichier dataset.csv
def getDataFromCSV(file):
    data = pd.read_csv(file, delimiter=';')
    return data

def cleanDataSet(data):
    data = data.dropna()
    for index, row in data.iterrows():
        data['Slowness in traffic (%)'][index] = row['Slowness in traffic (%)'].replace(',', '.')
    data['Slowness in traffic (%)'] = pd.to_numeric(data['Slowness in traffic (%)'], errors='coerce')
    data = data.sort_values(by=['Slowness in traffic (%)'])
    return data

def drawBarChart(data, title, size_x, size_y):
    fig, ax = plt.subplots(figsize=(size_x, size_y))
    plt.suptitle(title)
    data.plot.bar(ax=ax)
    plt.show()

def drawPieChart(data, title, size_x, size_y):
    fig, ax = plt.subplots(figsize=(size_x, size_y))
    plt.suptitle(title)
    data.plot.pie(labels=data.index, autopct='%.0f%%')
    plt.show()


def main():

    data = getDataFromCSV(file)
    data = cleanDataSet(data)

    subset1 =  data[['Vehicle excess', 'Accident victim']]
    subset1_data = subset1.groupby(['Vehicle excess']).first()

    subset2 = data[['Fire vehicles', 'Accident victim']]
    subset2_data = subset2.groupby(['Fire vehicles']).first()

    subset3 = data[['Slowness in traffic (%)', 'Accident victim']]
    subset3_data = subset3.groupby(['Slowness in traffic (%)']).first()

    subset4 = data[['Broken Truck', 'Accident victim']]
    subset4_data = subset4.groupby(['Broken Truck']).first()

    subset5 = data['Accident victim']
    subset5_data = subset5.value_counts()

    subset6 = data[['Defect in the network of trolleybuses', 'Accident victim']]
    subset6_data = subset6.groupby(['Defect in the network of trolleybuses']).first()

    drawBarChart(subset1_data, 'Victimes d\'incident en fonction de la lenteur du trafic', 20, 10)
    drawBarChart(subset4_data, 'Victimes d\'incident impliquant un camion accidenté', 10, 10)
    drawBarChart(subset6_data, 'Victimes d\'incident impliquant un défaut dans les Trolleybuses', 10, 10)
    drawPieChart(subset5_data, 'Répartition des victimes par incident', 10, 10)

main()